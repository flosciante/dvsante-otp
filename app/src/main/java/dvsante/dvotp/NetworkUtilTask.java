package dvsante.dvotp;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.telephony.TelephonyManager;

public class NetworkUtilTask {
    Context context;

    public NetworkUtilTask(Context context){
        this.context = context;
    }

    public String typeConnection() {

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo reseauMobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if(reseauMobile==null || !reseauMobile.isConnected())
            return "-"; //not connected
        if(reseauMobile.getType() == ConnectivityManager.TYPE_WIFI)
            return "WIFI";
        if(reseauMobile.getType() == ConnectivityManager.TYPE_MOBILE){
            int networkType = reseauMobile.getSubtype();
            switch (networkType) {
                case TelephonyManager.NETWORK_TYPE_GPRS:
                case TelephonyManager.NETWORK_TYPE_EDGE:
                case TelephonyManager.NETWORK_TYPE_CDMA:
                case TelephonyManager.NETWORK_TYPE_1xRTT:
                case TelephonyManager.NETWORK_TYPE_IDEN: //api<8 : replace by 11
                    return "2G";
                case TelephonyManager.NETWORK_TYPE_UMTS:
                case TelephonyManager.NETWORK_TYPE_EVDO_0:
                case TelephonyManager.NETWORK_TYPE_EVDO_A:
                case TelephonyManager.NETWORK_TYPE_HSDPA:
                case TelephonyManager.NETWORK_TYPE_HSUPA:
                case TelephonyManager.NETWORK_TYPE_HSPA:
                case TelephonyManager.NETWORK_TYPE_EVDO_B: //api<9 : replace by 14
                case TelephonyManager.NETWORK_TYPE_EHRPD:  //api<11 : replace by 12
                case TelephonyManager.NETWORK_TYPE_HSPAP:  //api<13 : replace by 15
                    return "3G";
                case TelephonyManager.NETWORK_TYPE_LTE:    //api<11 : replace by 13
                    return "4G";
                default:
                    return "?";
            }
        }
        return "?";
    }



    public boolean hasActiveInternetConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();

        Boolean actif = true;

        if (netInfo != null && netInfo.isConnected()) {
            //System.out.println("wifi ok");

            actif = true;
            //Log.println(Log.ASSERT, "wifi", "OK");
        } else
        {
            //System.out.println("wifi ko");
            actif = false;
            //Log.println(Log.ASSERT, "wifi", " KO");
        }

        return actif;
    }

    public boolean hasActiveMobileConnection(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo reseauMobile = cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        String typeConnection = typeConnection();

        System.out.println(typeConnection);

        Boolean actif = true;

        if (reseauMobile != null && reseauMobile.isConnected()) {
            actif = true;
            //Log.println(Log.ASSERT, "réseau mobile", "OK");
        } else
        {
            actif = false;
            //Log.println(Log.ASSERT, "réseau mobile", " KO");
        }

        return actif;
    }

}
