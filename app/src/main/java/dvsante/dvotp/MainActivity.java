package dvsante.dvotp;

import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.provider.Settings.Secure;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;
import com.crashlytics.android.Crashlytics;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import dvsante.dvotp.mailSender.GMailSender;
import io.fabric.sdk.android.Fabric;

public class MainActivity extends AppCompatActivity {

    private Handler mHandler = new Handler();
    NetworkUtilTask checkNetWork;
    TextView logTxView;
    String log;

    /** The Progress dialog. */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        Fabric.with(this, new Crashlytics());

        logTxView = (TextView) findViewById(R.id.log);
        logTxView.setMovementMethod(new ScrollingMovementMethod());

        new LongOperation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    public String getDateHours() {
        String currentDateandTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
        return currentDateandTime;
    }

    public void setLog(final String logStr) {
        MainActivity.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                logTxView.setText(logStr);
            }
        });
    }

    public void checkSms() {
        //test d'abord si device déjà répertorié
        String json;
        GetAsyncWs connect = new GetAsyncWs();
        //connect.setUrl("http://wspatient.monali.fr/1.0/index.php/usersession/connectUserAction/");
        connect.setUrl("http://dv-api.itmnl.com/1.0/index.php/usersession/getSmsToSendAction/");
        HashMap<String, String> params = new HashMap<>();
        params.put("safetyKey", "sftykmnldv");

        connect.setParamsValue(params);
        json = connect.doInBackground();

        JSONObject jsonObj = null;
        String codeSent = null;
        String number = null;
        String id_activationCode;

        if (json != null) {
            try {
                jsonObj = new JSONObject(json);
                JSONArray resutats;


                // Getting JSON Array node
                resutats = jsonObj.getJSONArray("results");

                for (int i = 0; i < resutats.length(); i++) {

                    JSONObject otp = resutats.getJSONObject(i);

                    codeSent = otp.getString("code");
                    number = otp.getString("phoneNumber");
                    id_activationCode = otp.getString("id_activationCode");

                    sendSms(number, codeSent, id_activationCode);
                }


            } catch (JSONException e) {
                log = logTxView.getText().toString() + getDateHours() + " -> ERREUR Exception sur getSmsToSendAction \n\n";
                setLog(log);
                e.printStackTrace();
            }
        } else {
            log = logTxView.getText().toString() + getDateHours() + " -> ERREUR JSON null sur getSmsToSendAction \n\n";
            setLog(log);
        }
    }


    public void sendSms(final String number, final String codeSent, final String idActivationCode) {

        try {
            SmsManager smsManager = SmsManager.getDefault();
            smsManager.sendTextMessage(number, null, "Voici le code d'identification pour vous connecter à l'application DVSanté :\n"+codeSent, null, null);

            String json;
            GetAsyncWs connect = new GetAsyncWs();
            //connect.setUrl("http://wspatient.monali.fr/1.0/index.php/usersession/connectUserAction/");
            connect.setUrl("http://dv-api.itmnl.com/1.0/index.php/usersession/setSmsIsSentAction/");
            HashMap<String, String> params = new HashMap<>();
            params.put("id_activationCode", idActivationCode);

            connect.setParamsValue(params);
            json = connect.doInBackground();

            System.runFinalization();
            Runtime.getRuntime().gc();
            System.gc();

            log = logTxView.getText().toString() + getDateHours() + " -> Envoi d'un sms au : " + number + "\n Code envoyé : " + codeSent + "\n\n";
            setLog(log);

        } catch (Exception ex) {

            log = logTxView.getText().toString() + getDateHours() + " -> ECHEC d'envoi d'un sms au : " + number + "\n Code envoyé : " + codeSent + "\n\n";
            setLog(log);
            sendMailException(ex);
        }
    }

    public void sendMailException(Exception e) {

        String stakeTrace = "";

        try {
            for (int i = 0; i < e.getStackTrace().length; i++) {
                stakeTrace += "\n" + e.getStackTrace()[i];
            }

            GMailSender sender = new GMailSender("fdelerue@monali.fr", "flo62rent44");
            sender.sendMail("Erreur otp sms",
                    e.getMessage() + "\n" + stakeTrace,

                    "fdelerue@monali.fr", "fdelerue@monali.fr");
        } catch (Exception exMail) {

            Log.e("SendMail", e.getMessage(), e);
        }
    }

    public void checkNetWork(NetworkUtilTask networkUtilTask, final ProgressDialog dialog, Boolean isNetWork) {

        if (networkUtilTask.hasActiveInternetConnection(getApplicationContext()) == false && networkUtilTask.hasActiveMobileConnection(getApplicationContext()) == false) {
            isNetWork = false;
            MainActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        dialog.setMessage("Oops, il semble que l'application n'accède plus à internet, veuillez vérifier vos connexions");
                        dialog.show();
                    } catch(Exception e) {
                        Toast.makeText(getApplicationContext(),"Il semble que l'application n'accède plus à internet," +
                                " veuillez vérifier vos connexions", Toast.LENGTH_LONG);
                    }
                }
            });

        } else {
            if (dialog.isShowing()) {
                MainActivity.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        dialog.dismiss();
                    }
                });
            }
        }
    }

    private class LongOperation extends AsyncTask<Object, Void, String[]> {

        final ProgressDialog dialog = new ProgressDialog(MainActivity.this);
        final NetworkUtilTask checkNetWork = new NetworkUtilTask(getApplicationContext());
        final boolean isNetWork = true;

        @Override
        protected String[] doInBackground(Object... params) {

            boolean b = true;

            while (b) {

                try {
                    Thread.sleep(10000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                checkNetWork(checkNetWork, dialog, isNetWork);
                // TODO Auto-generated method stub


                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        log = logTxView.getText().toString();

                        if (logTxView.getText().length() == 20000) {
                            logTxView.setText("");
                        }
                    }
                });

                log = log + getDateHours() + " -> Check sms \n\n";
                setLog(log);
                checkSms();

            }

            return new String[0];
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected void onPostExecute(String[] strings) {
            super.onPostExecute(strings);
            new LongOperation().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
        }
// etc etc etc
    }
}
