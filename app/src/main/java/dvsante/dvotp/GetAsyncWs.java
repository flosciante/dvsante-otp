package dvsante.dvotp;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by florentdelerue on 20/01/2016.
 */
public class GetAsyncWs extends AsyncTask<String, Void, String> {

    JSONParser jsonParser = new JSONParser();

    private HashMap<String, String> paramsValue;
    //private HashMap<String, Object> paramsValueList;
    private String url;
    private Activity activityCurrent;
    private ProgressDialog progressBar;


    protected void onPostExecute(JSONObject json) {

    }

    public GetAsyncWs(Activity activity) {

        this.activityCurrent = activity;

    }
    public GetAsyncWs() {

    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        if (progressBar.isShowing()) {
            progressBar.dismiss();
        }
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

        if (activityCurrent != null) {
            progressBar = new ProgressDialog(activityCurrent);
            progressBar.setMessage("Downloading...");
            progressBar.show();
        }

    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    public String doInBackground(String... parametre) {

        try {

            String jsonString;
            HashMap<String, String> params = new HashMap<>();

            for(Map.Entry<String, String> entry : paramsValue.entrySet()) {

                String cle = entry.getKey();
                String valeur = entry.getValue();

                params.put(cle, valeur);
            }

            JSONObject json = jsonParser.makeHttpRequest(
                    url, "POST", params);

            if (json != null) {
                jsonString = json.toString();

                return jsonString;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public HashMap<String, String> getParamsValue() {
        return paramsValue;
    }

    public void setParamsValue(HashMap<String, String> paramsValue) {
        this.paramsValue = paramsValue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
